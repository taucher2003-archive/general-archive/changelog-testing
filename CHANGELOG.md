## 1.0.2 (2021-09-12)

### changed (1 change)

- [Add new file](taucher2003-group/changelog-testing@a4c98c4d6449abd1e18fce25011f6b8b1205d96e)

### added (3 changes)

- [Add new filefasdfasfda](taucher2003-group/changelog-testing@cb066b5cf13f0947b7505e48bdadb410da6e92c3) ([merge request](taucher2003-group/changelog-testing!4))
- [Add new fileafadfa](taucher2003-group/changelog-testing@292047f51b2c888473bfe7a27675277e2e538986) ([merge request](taucher2003-group/changelog-testing!3))
- [Add new file](taucher2003-group/changelog-testing@b01be96fb8be9b10e6f25a4a3e1e2c1d18f0c971)

## 1.0.1 (2021-09-12)

No changes.

## 1.0.0 (2021-09-12)

No changes.
